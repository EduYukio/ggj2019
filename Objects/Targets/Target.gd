extends Area2D

export var id = "NUMBER"

func _on_Target_area_entered(area):
	var node_collided = area.get_parent()
	if(node_collided.get_name() == "Player" and node_collided.item_held != null and self.id != null):
		if(self.id == node_collided.item_held.id):
			node_collided.can_use_on_target = true
			node_collided.target_node = self
	if (node_collided.get_name() == "Player"):
		node_collided.is_on_interactable = true

func _on_Target_area_exited(area):
	var node_collided = area.get_parent()
	if(node_collided.get_name() == "Player"):
		node_collided.can_use_on_target = false
		node_collided.target_node = null
		node_collided.is_on_interactable = false
