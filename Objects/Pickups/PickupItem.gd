extends Area2D

export var id = "NUMBER"

func _on_PickupItem_area_entered(area):
	var node_collided = area.get_parent()
	if(node_collided.get_name() == "Player"):
		node_collided.can_pickup_item = true
		node_collided.item_node = self

func _on_PickupItem_area_exited(area):
	var node_collided = area.get_parent()
	if(node_collided.get_name() == "Player"):
		node_collided.can_pickup_item = false
#		node_collided.item_node = null
