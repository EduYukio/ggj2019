extends KinematicBody2D

var map # Phase Tilemap, must be initialized before
var map_size = Vector2(27, 20)
var astar_node = AStar.new()
var path = []
var prevPos = Vector2()
var velocity = Vector2()
var rest_points = []
var actual_rest_point = 0
var walking_to_kid = false

onready var obstacles = map.get_used_cells_by_id(0)

func get_point_index(point):
	var retVal = - 1
	if not is_outside_map(point):
		retVal = point.x + map_size.x * point.y
	return retVal

func get_point_from_index(index):
	var x =int(index)%int(map_size.x)
	var y = int(index - x)/int(map_size.x)
	return Vector2(x, y)

func is_outside_map(point):
	return point.x < 0 or point.y < 0 or point.x >= map_size.x or point.y >= map_size.y

func _ready():
	var walkable_points = create_walkable_points(obstacles)
	connect_walkable_points(walkable_points)

func _process(delta):
	var window_size = OS.get_window_size()
	var kid_pos_viewport = get_node("../Player/Camera2D").get_camera_screen_center() - position + window_size/2
	get_node("..").material.set_shader_param("mom_pos", Vector2(window_size.x - kid_pos_viewport.x, kid_pos_viewport.y))
	get_node("../../HUD/MomAura").material.set_shader_param("mom_pos", Vector2(window_size.x - kid_pos_viewport.x, kid_pos_viewport.y))

	if path.size() > 0:
		if (path[0] - position).length() < 3.5:
			position = path[0]
			path.remove(0)
		if path.size() > 0:
			prevPos = position
			velocity = Vector2(path[0].x - position.x, path[0].y - position.y).normalized() * 100
			move_and_slide(velocity)
		else:
			velocity = Vector2()
			arrived()
	else:
		velocity = Vector2()

	if velocity:
		if $Sprite/AnimationPlayer.current_animation != "walk":
			$Sprite/AnimationPlayer.play("walk")
		rotation = velocity.angle() + PI/2
	else:
		$Sprite/AnimationPlayer.play("idle")


func create_walkable_points(obstacles = []):
	var points_array = []
	for y in range(map_size.y):
		for x in range(map_size.x):
			var point = Vector2(x, y)
			if point in obstacles:
				continue
			points_array.append(point)
			var point_index = get_point_index(point)
			astar_node.add_point(point_index, Vector3(point.x, point.y, 0.0))
	return points_array

func is_valid_neighbor(point_id):
	var retVal = point_id > 0 and astar_node.has_point(point_id)
	return retVal

func connect_walkable_points(points_array):
	for point in points_array:
		var point_index = get_point_index(point)

		var point_ids = [get_point_index(Vector2(point.x + 1, point.y)),
						get_point_index(Vector2(point.x, point.y - 1)),
						get_point_index(Vector2(point.x - 1, point.y)),
						get_point_index(Vector2(point.x, point.y + 1))]

		var directions = [is_valid_neighbor(point_ids[0]),  # right
						is_valid_neighbor(point_ids[1]), # top
						is_valid_neighbor(point_ids[2]), # left
						is_valid_neighbor(point_ids[3])] # down

		var diagonals = [directions[0] and directions[1], # top right
						directions[1] and directions[2], # top left
						directions[2] and directions[3], # bottom left
						directions[3] and directions[0]] # bottom right

		for i in range(4):
			if directions[i]:
				astar_node.connect_points(point_index, point_ids[i], false)
			if diagonals[i]:
				var diag_pt_id
				if i == 0:
					diag_pt_id = get_point_index(Vector2(point.x + 1, point.y - 1))
				if i == 1:
					diag_pt_id = get_point_index(Vector2(point.x - 1, point.y - 1))
				if i == 2:
					diag_pt_id = get_point_index(Vector2(point.x - 1, point.y + 1))
				if i == 3:
					diag_pt_id = get_point_index(Vector2(point.x + 1, point.y + 1))

				if is_valid_neighbor(diag_pt_id):
					astar_node.connect_points(point_index, diag_pt_id, false)

func walk_to(world_target_point, kid=false):
	$IdleTimer.paused = true
	$KidTimer.stop()
	walking_to_kid = kid
	var pos = position
	var target_point_index = get_point_index(map.world_to_map(world_target_point))
	var start_point_index = get_point_index(map.world_to_map(position))
	var tmp_path = astar_node.get_point_path(start_point_index, target_point_index)
	tmp_path.remove(0)
	path = []
	for pt in tmp_path:
		path.append(map.map_to_world(Vector2(pt.x, pt.y)) + Vector2(32, 32))

func arrived():
	if walking_to_kid:
		get_node("Imrighthere").play()
		$KidTimer.start()
	else:
		$IdleTimer.paused = false
		if $IdleTimer.is_stopped():
			$IdleTimer.start()
	walking_to_kid = false

func _on_IdleTimer_timeout():
	if rest_points.size() > 0:
		actual_rest_point = (actual_rest_point + 1)%rest_points.size()
		walk_to(rest_points[actual_rest_point])

func _on_KidTimer_timeout():
	if rest_points.size() > 0:
		walk_to(rest_points[actual_rest_point])
