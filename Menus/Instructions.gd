extends Control

const GAME_PATH = "res://Maps/Stage1.tscn"

func _ready():
	$LetsGo.grab_focus()

func _on_LetsGo_pressed():
	get_tree().change_scene(GAME_PATH)
