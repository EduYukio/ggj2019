extends Control

const CREDITS_PATH = "res://Menus/Credits.tscn"
const INSTRUCTIONS_PATH = "res://Menus/Instructions.tscn"

func _ready():
	$Buttons/Play.grab_focus()

func _on_Play_pressed():
	get_tree().change_scene(INSTRUCTIONS_PATH)

func _on_Credits_pressed():
	get_tree().change_scene(CREDITS_PATH)

func _on_Quit_pressed():
	get_tree().quit()
