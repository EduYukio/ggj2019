extends KinematicBody2D

var direction = Vector2(0,0)
var velocity = Vector2()
var speed = 200
var can_pickup_item = false
var item_node = null
var item_held = null
var target_node = null
var already_picked_item = false
var can_use_on_target = false
var is_on_interactable = false
var mom = null
var died = false
var lives = 3
var targets_remaining = 3
onready var cry_timer = $CryTimer
onready var hud_node = get_node("../../HUD")

onready var cry_cooldown_node = hud_node.get_node("CryCooldown")

onready var item_holder_node = hud_node.get_node("ItemHolder")
onready var walls_node = get_parent()

func _process(delta):
	process_interact_input()
	process_cry_input()

	update_direction()

	var window_size = OS.get_window_size()
	var kid_pos_viewport = $Camera2D.get_camera_screen_center() - position + window_size/2
	get_parent().material.set_shader_param("kid_pos", Vector2(window_size.x - kid_pos_viewport.x, kid_pos_viewport.y))

	if mom:
		if (mom.position - position).length() > 400:
			if $CatTimer.time_left == 0 and not died:
				start_cat_timer()
		else:
			reset_cat_timer()
			
	var timeleft = cry_timer.time_left
	if timeleft == 0.0:
		timeleft = cry_timer.wait_time
	var progress = timeleft/cry_timer.wait_time
	cry_cooldown_node.value = progress*100.0

func _physics_process(delta):
	walk()

func walk():
	velocity = direction.normalized()*speed
	move_and_slide(velocity)

func update_direction():
	direction = Vector2(0,0)

	if Input.is_action_pressed("ui_up"):
		direction.y -= 1

	if Input.is_action_pressed("ui_down"):
		direction.y += 1

	if Input.is_action_pressed("ui_left"):
		direction.x -= 1

	if Input.is_action_pressed("ui_right"):
		direction.x += 1

	if direction:
		if $Sprite/AnimationPlayer.current_animation != "Walk":
			$Sprite/AnimationPlayer.play("Walk")
		rotation = direction.angle() + PI/2

	else:
		$Sprite/AnimationPlayer.play("Idle")

func start_cat_timer():
	$CatTimer.start()

func reset_cat_timer():
	$CatTimer.stop()

func process_interact_input():
	if(Input.is_action_just_pressed("ui_accept")):
		if(can_pickup_item):
			get_node("PickUpItem").play()
			pick_item()
			item_held = item_node
		elif(can_use_on_target):
			get_node("InteractionSuccess").play()
			use_on_target()
		elif (not can_use_on_target and is_on_interactable):
			get_node("InteractionFailure").play()

func pick_item():
	if(can_pickup_item and item_node != null and item_holder_node != null):
		if(not already_picked_item):
			already_picked_item = true
		else:
			for c in item_holder_node.get_children():
				if(c is Area2D):
					c.get_node("Sprite").scale /= 2
					call_deferred("reparent_to_walls", c)

		call_deferred("reparent_to_holder", item_node)
		item_node.get_node("Sprite").scale *= 2

func reparent_to_holder(node):
	node.get_parent().remove_child(node)
	item_holder_node.add_child(node)
	node.set_owner(item_holder_node)
	node.position = Vector2()

func reparent_to_walls(node):
	node.get_parent().remove_child(node)
	walls_node.add_child(node)
	node.set_owner(walls_node)
	node.set_global_position(item_node.global_position)

func use_on_target():
	if(can_use_on_target and item_node != null and target_node != null):
		item_held.queue_free()
		target_node.queue_free()
		item_held = null
		target_node = null
		targets_remaining -= 1
		if(targets_remaining == 0):
					get_tree().change_scene("res://Menus/YouWin.tscn")

func process_cry_input():
	if Input.is_action_just_pressed("cry") and get_node("CryTimer").is_stopped():
		get_node("../Mother").walk_to(position, true)
		get_node("KidCrying").play()
		get_node("CryTimer").start()

func reset_variables():
	can_pickup_item = false
#	item_node = null
#	item_held = null
	target_node = null
	can_use_on_target = false
	is_on_interactable = false
	died = false

func _on_CatTimer_timeout():
	died = true
	reset_variables()
	get_node("../..").respawn()
