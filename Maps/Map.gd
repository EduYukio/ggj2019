extends Node2D

onready var Mom = preload("res://Mother/Mother.tscn")
onready var hud_tween = $HUD/Tween
onready var hud_rect = $HUD/ColorRect
onready var cat_timer = $Walls/Player/CatTimer
onready var hud_cat_progress = $HUD/CatProgress
onready var player = $Walls/Player

signal respawned

func _ready():
	fade_out()

	var mom = Mom.instance()
	mom.map = get_node("MomWalls")
	mom.set_position(Vector2(100, 100))
	player.mom = mom
	mom.rest_points = [Vector2(100, 100), Vector2(140, 720), Vector2(300, 720)]
	get_node("Walls").add_child(mom)

func _process(delta):
	var tl = cat_timer.time_left
	if tl == 0.0:
		tl = cat_timer.wait_time
	var progress = 1 - tl/cat_timer.wait_time
	hud_cat_progress.value = progress*100.0

func change_child_position_or_die():
	yield(get_tree(), 'idle_frame')
	player.position = get_node("Walls/Mother").position + 32*(player.position - get_node("Walls/Mother").position).normalized()
	yield(get_tree(), 'idle_frame')
	player.died = false
	emit_signal("respawned")

func respawn():
	get_node("HUD/Lives/Heart" + str(player.lives) + "/Sprite/AnimationPlayer").play("tear")
	player.lives -= 1
	fade_screen_and_execute("change_child_position_or_die", self, "respawned")

func fade_out():
	var initial_color = hud_rect.color
	var final_color = hud_rect.color
	final_color.a = 0.0
	hud_tween.interpolate_property(hud_rect, "color", initial_color, final_color, 1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	hud_tween.start()

func fade_in():
	var initial_color = hud_rect.color
	var final_color = hud_rect.color
	final_color.a = 1.0
	hud_tween.interpolate_property(hud_rect, "color", initial_color, final_color, 1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	hud_tween.start()

func fade_screen_and_execute(function_name, target, signal_name):
	get_node("Cat").play()
	var initial_color = hud_rect.color
	var final_color = hud_rect.color
	final_color.a = 1.0
	hud_tween.interpolate_property(hud_rect, "color", initial_color, final_color, 1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	hud_tween.start()
	yield(hud_tween, "tween_completed")

	if(player.lives == 0):
		get_tree().change_scene("res://Menus/GameOver.tscn")
	else:
		funcref(self, function_name).call_func()
		yield(target, signal_name)
		hud_tween.interpolate_property(hud_rect, "color", final_color, initial_color, 1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		hud_tween.start()
		yield(hud_tween, "tween_completed")
