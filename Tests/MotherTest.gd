extends Node2D

onready var Mom = preload("res://Mother/Mother.tscn")

func _ready():
	var mom = Mom.instance()
	mom.map = get_node("Walls")
	mom.set_position(Vector2(50, 50))
	get_node("Walls").add_child(mom)
	mom.walk_to(Vector2(100, 100))
